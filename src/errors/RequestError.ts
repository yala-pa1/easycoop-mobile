export default class RequestError {
  constructor(public message: string, public statusCode: number) {}
}
