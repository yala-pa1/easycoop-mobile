import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ProductForm from '../screens/ProductForm';
import colors from '../styles/colors';
import ProductsHome from '../screens/ProductsHome';
import ProductDetails from '../screens/ProductDetails';
import ProductTypeForm from '../screens/ProductTypeForm';
import ProducerForm from '../screens/ProducerForm';

export type StackScreens = {
  ProductDetails: { productId: string; producerId: string };
  ProductsHome: { producerId: string };
  ProductForm:
    | { isEditing?: boolean; productId?: string; producerId?: string }
    | undefined;
  ProductTypeForm: undefined | { productTypeName?: string };
  ProducerForm: undefined;
};

const Stack = createStackNavigator<StackScreens>();

const AuthRoutes: React.FC = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: '#FFF',
        headerShown: false,
        headerStyle: {
          backgroundColor: colors.mainGreen,
        },
      }}>
      <Stack.Screen component={ProducerForm} name="ProducerForm" />
      <Stack.Screen component={ProductsHome} name="ProductsHome" />
      <Stack.Screen component={ProductForm} name="ProductForm" />
      <Stack.Screen component={ProductDetails} name="ProductDetails" />
      <Stack.Screen component={ProductTypeForm} name="ProductTypeForm" />
    </Stack.Navigator>
  );
};

export default AuthRoutes;
