import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import style from './styles';
import { Product } from '../../models/Product';
import Ripple from 'react-native-material-ripple';

type Props = {
  product: Product;
  onItemClick(productId: string): void;
};

const ProductCard: React.FC<Props> = ({ product, onItemClick }) => {
  return (
    <View>
      <Ripple onPress={() => onItemClick(product._id)} style={style.card}>
        <View>
          <Text style={style.name}>{product.name}</Text>
          <Text style={style.desc}>{product.description}</Text>
        </View>
        <View>
          <Text
            style={{
              fontSize: 30,
              lineHeight: 30,
              fontWeight: 'normal',
              color: '#2eb364',
            }}>
            {'›'}
          </Text>
        </View>
      </Ripple>
    </View>
  );
};

export default ProductCard;
