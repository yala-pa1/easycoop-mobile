import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  card: {
    backgroundColor: 'white',
    borderLeftColor: '#2eb364',
    borderLeftWidth: 10,
    marginHorizontal: '2%',
    marginVertical: '2%',
    padding: '3%',
    borderTopEndRadius: 10,
    borderBottomEndRadius: 10,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  name: {},
  desc: {
    color: 'gray',
  },
});
