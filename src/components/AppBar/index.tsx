import React from 'react';
import { Appbar } from 'react-native-paper';
import colors from '../../styles/colors';
import { StackNavigationProp } from '@react-navigation/stack';
import { StackScreens } from '../../routes';
import { StackActions } from '@react-navigation/native';
import * as SecureStorage from 'expo-secure-store';

type AppBarProps = {
  navigation: StackNavigationProp<StackScreens>;
  canGoBack: boolean;
  canLogout: boolean;
  title: string;
};

const AppBar: React.FC<AppBarProps> = ({ navigation, canGoBack, title, canLogout }) => {
  const goBack = () => navigation.pop();
  const logout = async () => {
    await SecureStorage.deleteItemAsync('user_uid');
    await SecureStorage.deleteItemAsync('user_pass');
    await SecureStorage.deleteItemAsync('user_cpf');
    navigation.dispatch(StackActions.replace('ProducerForm'));
  }
  return (
    <Appbar.Header style={{ backgroundColor: colors.mainGreen, height: 50 }}>
      {canGoBack && <Appbar.BackAction onPress={() => goBack()} />}
      <Appbar.Content title={title} />
      {canLogout && <Appbar.Action icon="logout" onPress={() => logout()} />}
    </Appbar.Header>
  );
};

export default AppBar;
