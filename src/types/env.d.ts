declare module '@env' {
  export const API_BASE: string;
  export const REACT_APP_apiKey: string;
  export const REACT_APP_authDomain: string;
  export const REACT_APP_databaseURL: string;
  export const REACT_APP_projectId: string;
  export const REACT_APP_storageBucket: string;
  export const REACT_APP_messagingSenderId: string;
  export const REACT_APP_appId: string;
  export const REACT_APP_measurementId: string;
}
