import { ProductType, ProductTypeDetails } from '../models/ProductType';
import API from './api';
import RequestError from '../errors/RequestError';
import fb from './firebase';

const db = fb.firestore();

export default {
  async getAll(): Promise<ProductType[]> {
    try {
      const types = await db.collection('types').orderBy('name', 'asc').get();

      return types.docs.map(e => {
        return {
          _id: e.id,
          name: e.data().name,
          imageUrl: e.data().imageUrl,
        };
      });
    } catch (error) {
      throw new RequestError(
        'It was not possible to get the types of products',
        error.response.status,
      );
    }
  },
  async getOne(id: string): Promise<ProductType> {
    try {
      const type = await db.collection('types').doc(id).get();
      return {
        _id: id,
        name: type.data()?.name,
        imageUrl: type.data()?.imageUrl,
      };
    } catch (error) {
      throw new RequestError(
        'It was not possible to get the product type',
        error.response.status,
      );
    }
  },
};
