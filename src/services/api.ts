import axios from 'axios';

const API = axios.create({
  baseURL: 'https://easycoop-349a6-default-rtdb.firebaseio.com/',
});

export default API;
