import firebase from 'firebase/app';
import 'firebase/storage';
import {
  REACT_APP_apiKey,
  REACT_APP_appId,
  REACT_APP_authDomain,
  REACT_APP_databaseURL,
  REACT_APP_measurementId,
  REACT_APP_messagingSenderId,
  REACT_APP_projectId,
  REACT_APP_storageBucket,
} from '@env';

const firebaseConfig = {
  apiKey: REACT_APP_apiKey,
  authDomain: REACT_APP_authDomain,
  databaseURL: REACT_APP_databaseURL,
  projectId: REACT_APP_projectId,
  storageBucket: REACT_APP_storageBucket,
  messagingSenderId: REACT_APP_messagingSenderId,
  appId: REACT_APP_appId,
  measurementId: REACT_APP_measurementId,
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app();
}

export default firebase;
