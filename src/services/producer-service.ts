import RequestError from '../errors/RequestError';
import { ProducerDetails } from '../models/Producer';
import { ProductDetails } from '../models/Product';
import fb from '../services/firebase';

const db = fb.firestore();
export default {
  async create(producer: ProducerDetails): Promise<void> {
    try {
      db.collection('producers')
        .doc(producer.userId)
        .set({
          ...producer,
          createdAt: new Date().toUTCString(),
        });
    } catch (error) {
      throw new RequestError(
        'It was not possible to create the producer',
        error.response.status,
      );
    }
  },
  async getOne(id: string) {
    try {
      const producer = await db.collection('producers').doc(id).get();
      return {
        _id: id,
        name: producer.data()?.name,
      };
    } catch (err) {
      throw new RequestError('asdas', 404);
    }
  },
};
