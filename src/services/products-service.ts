import RequestError from '../errors/RequestError';
import { Product, ProductDetails } from '../models/Product';
import productTypesService from './product-types-service';
import fb from './firebase';

const db = fb.firestore();

export default {
  async getAll(producerId: string): Promise<Product[]> {
    try {
      const prods = await fb
        .firestore()
        .collection('products')
        .where('active', '==', true)
        .where('producerId', '==', producerId)
        .get();
      const products = prods.docs.map(e => {
        return {
          _id: e.id,
          name: e.data().name,
          description: e.data().description,
        };
      });
      return products as Product[];
    } catch (error) {
      console.error(error);
      throw new RequestError(
        'It was not possible to get the products',
        error.response.status,
      );
    }
  },
  async getOne(id: string): Promise<Product> {
    try {
      const productFound = await db.collection('products').doc(id).get();
      const type = await productTypesService.getOne(
        productFound.data()?.productType,
      );

      return {
        _id: productFound.id,
        name: productFound.data()?.name,
        description: productFound.data()?.description,
        imgURL: type.imageUrl,
        date: productFound.data()?.date,
        amountType: productFound.data()?.amountType,
        amountValue: productFound.data()?.amountValue,
        productType: productFound.data()?.productType,
      } as Product;
    } catch (error) {
      throw new RequestError(
        'It was not possible to get the product',
        error.response.status,
      );
    }
  },
  async create(product: ProductDetails): Promise<void> {
    try {
      const db = fb.firestore();
      db.collection('products').add({
        ...product,
        active: true,
        date: product.date.toISOString(),
      });
    } catch (error) {
      throw new RequestError(
        'It was not possible to create the product',
        error.response.status,
      );
    }
  },
  async update(product: Product) {
    try {
      await db
        .collection('products')
        .doc(product._id)
        .update({
          ...product,
          date: product.date.toISOString(),
        });
    } catch (error) {
      console.error(error);
      throw new RequestError(
        'It was not possible to update the product',
        error.response.status,
      );
    }
  },
  async delete(productId: string) {
    try {
      await db.collection('products').doc(productId).update({
        active: false,
      });
    } catch (error) {
      console.error(error);
      throw new RequestError(
        'It was not possible to delete the product',
        error.response.status,
      );
    }
  },
};
