import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  greetings: {
    color: '#2eb364',
    textAlign: 'center',
    marginTop: '10%',
    fontSize: 18,
  },
  mainTitle: {
    paddingHorizontal: '2%',
    alignSelf: 'center',
    borderBottomColor: '#2eb364',
    borderBottomWidth: 2,
    marginBottom: 30,
  },
  textTitle: {
    textAlign: 'center',
    marginTop: '10%',
    borderBottomColor: '#2eb364',
    fontSize: 20,
  },
  addButton: {
    width: 70,
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 40,
    backgroundColor: '#ff0000',

    position: 'absolute',
    bottom: '2.5%',
    right: '5%',
  },
  addButtonText: {
    color: 'white',
    fontSize: 70,
    lineHeight: 80,
  },
});
