import 'react-native-gesture-handler';
import React, { useCallback } from 'react';
import { Text, View, FlatList, ActivityIndicator, Alert } from 'react-native';
import { useIsFocused, RouteProp } from '@react-navigation/native';
import { useState } from 'react';
import ProductCard from '../../components/ProductCard';
import style from './styles';
import { useEffect } from 'react';
import colors from '../../styles/colors';
import { RefreshControl } from 'react-native';
import { Product } from '../../models/Product';
import productsService from '../../services/products-service';
import { StackNavigationProp } from '@react-navigation/stack';
import { StackScreens } from '../../routes';
import Ripple from 'react-native-material-ripple';
import RequestError from '../../errors/RequestError';
import producerService from '../../services/producer-service';
import { StackActions } from '@react-navigation/native';
import AppBar from '../../components/AppBar';

type Props = {
  route: RouteProp<StackScreens, 'ProductsHome'>;
  navigation: StackNavigationProp<StackScreens>;
};

const ProductsHome: React.FC<Props> = ({ navigation, route }) => {
  const [products, setProducts] = useState<Product[]>([]);
  const [producer, setProducer] = useState<{ _id: string; name: string }>();
  const [isLoading, setIsLoading] = useState(false);

  const ref = useIsFocused();

  useEffect(() => {
    navigation.setOptions({
      title: 'Produtos',
    });
    loadProducer();
    loadProducts();
  }, [ref]);

  const loadProducer = useCallback(async () => {
    try {
      setIsLoading(true);
      const producer = await producerService.getOne(route.params.producerId);
      setProducer(producer);
    } catch (err) {}
  }, []);

  const loadProducts = useCallback(async () => {
    try {
      setIsLoading(true);
      const productsFetched = await productsService.getAll(
        route.params?.producerId,
      );
      setProducts(productsFetched);
    } catch (err) {
      console.error(err);
      if (err instanceof RequestError) {
        Alert.alert(
          'Não foi possível buscar os produtos',
          'Verifique sua conexão com a internet',
        );
      } else {
        Alert.alert(
          'Não foi possível buscar os produtos',
          'Algo inesperado aconteceu',
        );
      }
    } finally {
      setIsLoading(false);
    }
  }, []);

  return (
    <View style={{ height: '100%' }}>
      <AppBar
        canLogout={true}
        canGoBack={false}
        navigation={navigation}
        title="Produtos"
      />
      {isLoading ? (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <ActivityIndicator color={colors.mainGreen} size="large" />
        </View>
      ) : (
        <View style={{ flex: 1 }}>
          {products.length === 0 ? (
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
              <Text style={{ fontSize: 16 }}> Não há produtos cadastrados </Text>
            </View>
          ) : (
            <FlatList
              refreshControl={
                <RefreshControl
                  refreshing={isLoading}
                  onRefresh={async () => loadProducts()}
                />
              }
              ListHeaderComponent={
                <>
                  <Text style={style.greetings}>
                    Bem vindo, {producer?.name}
                  </Text>
                  <View style={style.mainTitle}>
                    <Text style={style.textTitle}>Produtos</Text>
                  </View>
                </>
              }
              data={products}
              keyExtractor={item => item._id}
              renderItem={({ item }) => (
                <ProductCard
                  product={item}
                  onItemClick={productId =>
                    navigation.push('ProductDetails', {
                      productId,
                      producerId: producer?._id!,
                    })
                  }
                />
              )}
            />
          )}
          <Ripple
            style={[style.addButton]}
            onPress={() =>
              navigation.push('ProductForm', { producerId: producer?._id })
            }>
            <Text style={style.addButtonText}>+</Text>
          </Ripple>
        </View>
      )}
    </View>
  );
};

export default ProductsHome;
