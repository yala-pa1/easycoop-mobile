import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  firstLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  image: {
    height: 180,
    marginTop: 10,
    marginBottom: 10,
  },
  product: {
    fontSize: 20,
    flex: 0.9,
  },
  row: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    marginTop: 5,
    borderWidth: 1,
    borderColor: '#CCC',
    borderRadius: 6,
    overflow: 'hidden',
  },
  container: {
    // height: '100%',
    padding: 20,
    backgroundColor: '#fefefe',
    margin: 9,
    borderWidth: 0.3,
    borderRadius: 6,
    overflow: 'hidden',
  },
});
