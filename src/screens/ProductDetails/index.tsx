import 'react-native-gesture-handler';
import React, { useCallback, useState } from 'react';
import { Text, View, Image, Pressable, Alert } from 'react-native';
import styles from './styles';
import { useEffect } from 'react';
import { Product } from '../../models/Product';
import productsService from '../../services/products-service';
import { StackNavigationProp } from '@react-navigation/stack';
import { StackScreens } from '../../routes';
import { useIsFocused, RouteProp } from '@react-navigation/native';
import RequestError from '../../errors/RequestError';
import AppBar from '../../components/AppBar';

type Props = {
  route: RouteProp<StackScreens, 'ProductDetails'>;
  navigation: StackNavigationProp<StackScreens>;
};

const ProductDetails: React.FC<Props> = ({ route, navigation }) => {
  const [product, setProduct] = useState({} as Product);
  const ref = useIsFocused();

  // TODO: fix the usecallbakc to update on pop
  useEffect(() => {
    (async () => {
      const product = await productsService.getOne(route.params.productId);
      setProduct(product);
    })();
  }, [ref]);

  const loadProduct = useCallback(async () => {
    const product = await productsService.getOne(route.params.productId);
    setProduct(product);
  }, [ref]);

  const setStyles = () => {
    navigation.setOptions({
      title: product.name,
    });
  };

  useEffect(() => {
    setStyles();
  }, [product, loadProduct]);

  const onProductDelete = useCallback(async () => {
    try {
      await productsService.delete(route.params.productId);
      Alert.alert('Produto deletado com sucesso!');
      navigation.pop();
    } catch (error) {
      if (error instanceof RequestError) {
        Alert.alert(
          'Não foi possível deletar o produto',
          'Verifique sua conexão com a internet',
        );
      } else {
        Alert.alert(
          'Não foi possível deletar o produto',
          'Algo inesperado aconteceu',
        );
      }
    }
  }, []);

  return (
    <>
      <AppBar
        canGoBack={true}
        canLogout={false}
        navigation={navigation}
        title={'Visualizar Produto'}
      />
      <View style={styles.container}>
        <View style={styles.firstLine}>
          <Text style={styles.product}>{product.name}</Text>
          <Pressable
            onPress={() =>
              navigation.navigate('ProductForm', {
                isEditing: true,
                productId: product._id,
                producerId: route.params?.producerId,
              })
            }>
            <Text style={{ color: '#0c5bd1' }}>Editar</Text>
          </Pressable>
          <Pressable onPress={() => onProductDelete()}>
            <Text style={{ color: '#E8102D' }}>Excluir</Text>
          </Pressable>
        </View>
        <Image
          style={styles.image}
          source={{
            uri: product.imgURL,
          }}
          resizeMode="cover"
        />
        <View style={styles.row}>
          <Text>Produto</Text>
          <Text>{product.name}</Text>
        </View>
        <View style={styles.row}>
          <Text>Descrição</Text>
          <Text>{product.description}</Text>
        </View>
      </View>
    </>
  );
};

export default ProductDetails;
