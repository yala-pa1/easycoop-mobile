import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  productFormScreenContainer: {
    padding: 20,
    backgroundColor: '#f0f0f0',
    flex: 1,
    paddingBottom: 500
  },
  productFormCard: {
    borderRadius: 12,
    backgroundColor: '#f8f8f8',
  },

  // image container
  imageContainer: {
    position: 'relative',
  },
  image: {
    height: 180,
    borderTopRightRadius: 12,
    borderTopLeftRadius: 12,
  },
  selectImageLabelContainer: {
    backgroundColor: '#00000077',
    height: 35,
    position: 'absolute',
    bottom: 0,
    width: '100%',
    justifyContent: 'center',
    paddingLeft: 24,
  },

  // geral
  fieldsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
  },

  // form fields container
  formFiedlsContainer: {
    paddingHorizontal: 18,
    paddingVertical: 12,
    // borderRadius: 12
  },
  datePickerContainer: {
    marginVertical: 5,
  },
  dateText: {
    backgroundColor: '#FFF',
    color: 'blue',
    padding: 5,
    alignSelf: 'flex-end',
  },
  input: {
    height: 40,
    //padding: 12,
    flex: 1,
    padding: 10,
    paddingRight: 30,
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 12,
    borderColor: '#CCC',
    backgroundColor: '#FFF',
  },
  textArea: {
    borderStyle: 'solid',
    justifyContent: 'flex-start',
    borderWidth: 1,
    borderRadius: 12,
    borderColor: '#CCC',
    backgroundColor: '#FFF',
    height: 100,
    padding: 8,
  },
  picker: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: '#CCC',
    flex: 0.9,
    height: 50,
    width: 200,
  },
  addTypeBtn: {
    backgroundColor: colors.mainGreen,
    padding: 12,
    borderRadius: 5,
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  addTypeBtnText: {
    color: '#FFF',
  },
  selectImageLabel: {
    color: '#F9F9F9',
    fontSize: 15,
  },
});
