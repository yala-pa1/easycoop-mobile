import React, { useEffect, useCallback, useState, useRef } from 'react';
import {
  ScrollView,
  View,
  Image,
  Text,
  Button,
  Platform,
  TextInput,
  Alert,
  ActivityIndicator,
  TouchableNativeFeedback,
} from 'react-native';
import { Picker } from '@react-native-community/picker';
import { useIsFocused, RouteProp } from '@react-navigation/native';
import AutoCompleteInput from 'react-native-autocomplete-input';
import { StackNavigationProp } from '@react-navigation/stack';
import styles from './styles';
import productTypesService from '../../services/product-types-service';
import { ProductType } from '../../models/ProductType';
import colors from '../../styles/colors';
import { StackScreens } from '../../routes';
import productsService from '../../services/products-service';
import RequestError from '../../errors/RequestError';
import { numberMask } from '../../utils/masks';
import { Product } from '../../models/Product';
import Ripple from 'react-native-material-ripple';
import AppBar from '../../components/AppBar';

type AmountType = 'Mass' | 'Quantity' | 'Liters' | 'Bag' | 'Box';

type Props = {
  route: RouteProp<StackScreens, 'ProductForm'>;
  navigation: StackNavigationProp<StackScreens>;
};

const ProductForm: React.FC<Props> = ({ navigation, route }) => {
  const [allProductTypes, setAllProductTypes] = useState<ProductType[]>([]);
  const [productType, setProductType] = useState<ProductType | undefined>(
    undefined,
  );
  const [amountType, setAmountType] = useState<AmountType>('Mass');
  const [mass, setMass] = useState<string>('');
  const [quantity, setQuantity] = useState<string>('');
  const [bag, setBag] = useState<string>('');
  const [box, setBox] = useState<string>('');
  const [liters, setLiters] = useState<string>('');
  const [date, setDate] = useState(new Date());
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [description, setDescription] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [dateText, setDateText] = useState('Selecionar data');
  const [autocompleteArray, setAutocompleteArray] = useState<ProductType[]>([]);
  const [typeNotExistis, setProductTypeNotExists] = useState(false);
  const [autoCompleteValue, setAutoCompleteValue] = useState('');
  const [descLen, setDescLen] = useState(0);
  const focus = useIsFocused();
  const scrollViewRef = useRef<ScrollView>(null);

  const loadProductTypes = async () => {
    try {
      setIsLoading(true);
      const productTypes = await productTypesService.getAll();
      setAllProductTypes(productTypes);
      if (route.params?.isEditing) {
        await populateForm();
      }
    } catch (e) {
      console.log(e);
    } finally {
      setIsLoading(false);
    }
  };

  const populateForm = async () => {
    const product = await productsService.getOne(route.params?.productId!);

    const amountTypesActions: { [key in AmountType]: () => void } = {
      Mass: () => setMass(product.amountValue?.toString()!),
      Quantity: () => setQuantity(product.amountValue?.toString()!),
      Liters: () => setLiters(product.amountValue?.toString()!),
      Bag: () => setBag(product.amountValue?.toString()!),
      Box: () => setBox(product.amountValue?.toString()!),
    };
    amountTypesActions[product.amountType as AmountType]();

    setAmountType(product.amountType?.toString() as AmountType);
    setDescLen(product.description.length);
    setDate(new Date(product.date));
    setDescription(product.description);

    const productType = await productTypesService.getOne(product.productType!);
    setProductType(productType);
    setAutoCompleteValue(productType.name);
  };

  const scrollToEnd = () => {
    if (scrollViewRef.current) {
      setTimeout(() => {
        scrollViewRef.current!.scrollToEnd();
      }, 500);
    }
  };

  useEffect(() => {
    setStyles();
    loadProductTypes();
  }, []);

  useEffect(() => {
    loadProductTypes();
  }, [focus]);

  useEffect(() => {
    setDateText(date.toLocaleString());
  }, [date]);

  const onFormSubmit = async () => {
    if (!validateForm()) {
      Alert.alert(
        'Formulário incompleto',
        'Por favor preencha todos os campos',
      );
      return;
    }
    try {
      const product = {
        date,
        description,
        producerId: route.params?.producerId,
        productType: productType?._id!,
        amountType,
        amountValue:
          amountType === 'Mass'
            ? Number(mass.replace('.', '').replace(',', '.'))
            : amountType === 'Liters'
            ? Number(liters)
            : amountType === 'Bag'
            ? Number(bag)
            : amountType === 'Box'
            ? Number(box)
            : Number(quantity),
        name: productType?.name,
        imgURL: productType?.imageUrl,
        _id: route.params?.isEditing ? route.params.productId! : undefined,
      };
      if (route.params?.isEditing) {
        await productsService.update(product as Product);
      } else {
        delete product._id;
        await productsService.create(product as Product);
      }
      Alert.alert('O Produto foi salvo com sucesso!');
      navigation.pop();
    } catch (error) {
      console.error(error);
      if (error instanceof RequestError) {
        Alert.alert(
          'Não foi possível salvar o produto',
          'Verifique sua conexão com a internet',
        );
      } else {
        Alert.alert(
          'Não foi possível salvar o produto',
          'Algo inesperado aconteceu',
        );
      }
    }
  };

  const validateForm = () => {
    return !!(
      ((amountType === 'Mass' && mass) ||
        (amountType === 'Quantity' && quantity) ||
        (amountType === 'Liters' && liters) ||
        (amountType === 'Bag' && bag) ||
        (amountType === 'Box' && box)) &&
      description &&
      date &&
      productType
    );
  };

  const onSelectDate = useCallback((_, selectedDate?: Date) => {
    setShowDatePicker(Platform.OS === 'ios');
    if (selectedDate) {
      setDate(selectedDate);
    }
  }, []);

  const setStyles = useCallback(() => {
    navigation.setOptions({
      title: route.params?.isEditing
        ? 'Atualizar Produto'
        : 'Cadastrar Produto',
    });
  }, []);

  const selectProductType = (productType: ProductType) => {
    setAutocompleteArray([]);
    setAutoCompleteValue(productType.name);
    setProductType(productType);
    setProductTypeNotExists(false);
  };

  const onChangeAutocomplete = (text: string) => {
    setAutoCompleteValue(text);
    setProductTypeNotExists(false);
    setProductType(undefined);
    if (text.length === 0) {
      setAutocompleteArray([]);
      return;
    }

    let typedExact = false;

    const autoCompleteTypes = allProductTypes.filter(type => {
      if (type.name.toLocaleUpperCase() === text.toLocaleUpperCase()) {
        setProductTypeNotExists(false);
        setAutocompleteArray([]);
        setProductType(type);
        typedExact = true;
        return true;
      }
      return type.name.toUpperCase().startsWith(text.toUpperCase());
    });
    if (!typedExact) {
      setAutocompleteArray(autoCompleteTypes);
    }
  };

  const onAutoCompleteBlur = () => {
    if (autoCompleteValue.length > 0 && !productType)
      setProductTypeNotExists(true);
  };

  const renderAutoCompleteItem = (productType: ProductType) => {
    return (
      <Ripple
        key={productType._id}
        onPress={() => selectProductType(productType)}>
        <Text style={{}}>{productType.name}</Text>
      </Ripple>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <AppBar
        canGoBack={true}
        canLogout={false}
        navigation={navigation}
        title={route.params?.isEditing ? 'Editar Produto' : 'Cadastrar Produto'}
      />
      {isLoading ? (
        <View
          style={{
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator color={colors.mainGreen} size="large" />
        </View>
      ) : (
        <ScrollView style={styles.productFormScreenContainer}>
          <View style={[styles.productFormCard]}>
            <View style={styles.imageContainer}>
              {productType ? (
                <Image
                  style={styles.image}
                  source={{
                    uri: productType.imageUrl,
                  }}
                  resizeMode="cover"
                />
              ) : (
                <View
                  style={{
                    height: 180,
                    backgroundColor: '#CFCFCF',
                    borderTopRightRadius: 12,
                    borderTopLeftRadius: 12,
                  }}
                />
              )}
              <View style={styles.selectImageLabelContainer}>
                <Text style={styles.selectImageLabel}>
                  {productType
                    ? `Tipo do produto: ${productType.name}`
                    : 'Tipo do produto não selecionado'}
                </Text>
              </View>
            </View>

            <View style={styles.formFiedlsContainer}>
              <View style={styles.fieldsRow}>
                <AutoCompleteInput
                  value={autoCompleteValue}
                  style={{
                    // ...styles.input, height: 50
                    borderWidth: 1,
                    borderRadius: 12,
                    borderColor: '#CCC',
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    marginBottom: 5,
                  }}
                  autoCorrect={true}
                  data={autocompleteArray}
                  inputContainerStyle={{ borderColor: '#FFF' }}
                  placeholder="Insira o tipo do produto"
                  onChangeText={text => onChangeAutocomplete(text)}
                  onBlur={() => onAutoCompleteBlur()}
                  flatListProps={{
                    keyExtractor: (_, idx) => String(idx),
                    renderItem: ({ item }) => renderAutoCompleteItem(item),
                  }}
                />
              </View>
              <View style={{ alignItems: 'center', marginBottom: 8 }}>
                {typeNotExistis && (
                  <Text style={{ color: 'red', fontSize: 14 }}>
                    O tipo do produto não existe
                  </Text>
                )}
                <View
                  style={{
                    alignSelf: 'flex-end',
                    backgroundColor: colors.mainGreen,
                    padding: 3,
                    borderRadius: 5,
                  }}>
                  <TouchableNativeFeedback
                    onPress={() =>
                      navigation.push('ProductTypeForm', {
                        productTypeName: autoCompleteValue,
                      })
                    }>
                    <Text
                      style={{
                        fontWeight: '700',
                        fontSize: 12,
                        color: '#FFF',
                      }}>
                      Adicionar novo tipo
                    </Text>
                  </TouchableNativeFeedback>
                </View>
              </View>
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginVertical: 2,
                }}>
                <Text>Tipo da medição:</Text>
                <Picker
                  selectedValue={amountType}
                  style={styles.picker}
                  onValueChange={itemValue => {
                    setAmountType(itemValue as AmountType);
                  }}>
                  <Picker.Item label="Quilo" value="Mass" />
                  <Picker.Item label="Unidade" value="Quantity" />
                  <Picker.Item label="Litro" value="Liters" />
                  <Picker.Item label="Saca" value="Bag" />
                  <Picker.Item label="Caixa" value="Box" />
                </Picker>
              </View>
              <View style={[styles.fieldsRow, { marginVertical: 2 }]}>
                {amountType === 'Mass' ? (
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <TextInput
                      style={{ ...styles.input, flex: 1 }}
                      value={route.params?.isEditing ? mass : numberMask(mass)}
                      maxLength={9}
                      textAlign="right"
                      placeholder={'Massa em quilos'}
                      onChangeText={mass => setMass(numberMask(mass))}
                      keyboardType="numeric"
                    />
                    <Text
                      style={{
                        padding: 10,
                        marginLeft: -35,
                      }}>
                      Kg
                    </Text>
                  </View>
                ) : amountType === 'Liters' ? (
                  <TextInput
                    value={liters?.toString()}
                    placeholder={'Litros'}
                    style={{ ...styles.input, paddingRight: 10 }}
                    textAlign="right"
                    onChangeText={liters => {
                      setLiters(liters);
                    }}
                    keyboardType="numeric"
                  />
                ) : amountType === 'Bag' ? (
                  <TextInput
                    value={bag?.toString()}
                    placeholder={'Saca'}
                    style={{ ...styles.input, paddingRight: 10 }}
                    textAlign="right"
                    onChangeText={bag => {
                      setBag(bag);
                    }}
                    keyboardType="numeric"
                  />
                ) : amountType === 'Box' ? (
                  <TextInput
                    value={box?.toString()}
                    placeholder={'Quantidade de caixas'}
                    style={{ ...styles.input, paddingRight: 10 }}
                    textAlign="right"
                    onChangeText={box => {
                      setBox(box);
                    }}
                    keyboardType="numeric"
                  />
                ) : (
                  <TextInput
                    value={quantity?.toString()}
                    placeholder={'Unidades do produto'}
                    style={{ ...styles.input, paddingRight: 10 }}
                    textAlign="right"
                    onChangeText={quantity => {
                      setQuantity(quantity);
                    }}
                    keyboardType="numeric"
                  />
                )}
              </View>
              <TextInput
                secureTextEntry={true}
                textAlignVertical="top"
                maxLength={200}
                multiline
                numberOfLines={8}
                value={description}
                onFocus={() => scrollToEnd()}
                placeholder={'Descrição'}
                style={[styles.textArea, { marginVertical: 2 }]}
                onChangeText={text => {
                  setDescLen(text.length);
                  setDescription(text);
                }}
              />
              <View
                style={{
                  alignSelf: 'flex-end',
                  paddingRight: 5,
                }}>
                <Text>{descLen}/200</Text>
              </View>
              <View style={{ marginTop: 16, marginBottom: 200 }}>
                <Button
                  color={colors.mainGreen}
                  disabled={isLoading}
                  title={route.params?.isEditing ? 'Atualizar' : 'Cadastrar'}
                  onPress={() => onFormSubmit()}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      )}
    </View>
  );
};

export default ProductForm;
