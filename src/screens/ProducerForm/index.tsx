import React from 'react';
import { useState, useEffect } from 'react';
import {
  ActivityIndicator,
  Alert,
  Button,
  ImageBackground,
  Text,
  View,
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import style from './styles';
import colors from '../../styles/colors';
import * as Location from 'expo-location';
import { cpfMask } from '../../utils/masks';
import producerService from '../../services/producer-service';
import { ProducerDetails } from '../../models/Producer';
import { RouteProp, StackActions, useNavigation } from '@react-navigation/native';
import * as ImagePicker from 'expo-image-picker';
import fb from '../../services/firebase';
import AppBar from '../../components/AppBar';
import { StackScreens } from '../../routes';
import { StackNavigationProp } from '@react-navigation/stack';
import * as SecureStorage from 'expo-secure-store';

type Props = {
  route: RouteProp<StackScreens, 'ProducerForm'>;
  navigation: StackNavigationProp<StackScreens>;
};

const ProducerForm: React.FC<Props> = ({ navigation, route }) => {
  const auth = fb.auth();
  const [imageUri, setImageUri] = useState('none');
  const [isLoading, setIsLoading] = useState(true);
  const [login, setLogin] = useState(true);
  const [name, setName] = useState('');
  const [cpf, setCpf] = useState('');
  const [pass, setPass] = useState('');
  const [lat, setLat] = useState(0);
  const [lon, setLon] = useState(0);

  const onFormLoginSubmit = async () => {
    try {
      if (pass.length === 0 || cpf.length === 0) {
        Alert.alert('Preencha suas credenciais');
        return;
      }
      await auth.signInWithEmailAndPassword(
        `${cpf}@easycoop.com`,
        pass,
      ).then(async (user) => {
        const producerId = user.user?.uid;
  
        await SecureStorage.setItemAsync('user_uid', `${user.user?.uid}`);
        await SecureStorage.setItemAsync('user_pass', pass);
        await SecureStorage.setItemAsync('user_cpf', cpf);
  
        navigation.dispatch(StackActions.replace('ProductsHome', { producerId }));
      }).catch(e => {
        Alert.alert(e);
      });
    } catch (err) {
    }
  };

  const onFormSubmit = async () => {
    try {
      setIsLoading(true);
      if (name.trim().length === 0 || name.trim().length < 3) {
        Alert.alert('O nome deve possuir no mínimo 3 caracteres');
        return;
      } 
      if (cpf.trim().length != 14) {
        Alert.alert('CPF incompleto');
        return;
      }
      if (pass.trim().length === 0 || pass.trim().length < 6) {
        Alert.alert('A senha deve possuir no mínimo 6 caracteres');
        return;
      }
      const user = await auth.createUserWithEmailAndPassword(
        `${cpf.trim()}@easycoop.com`,
        pass.trim(),
      );
      const producer: ProducerDetails = {
        userId: user.user?.uid!,
        name,
        cpf,
        location: {
          latitude: String(lat),
          longitude: String(lon),
        },
        logoImg: imageUri,
      };
      await producerService.create(producer);
      navigation.dispatch(
        StackActions.replace('ProductsHome', { producerId: user.user?.uid! }),
      );
    } catch (err) {
      if (err.code === 'auth/email-already-in-use') {
        Alert.alert('CPF já cadastrado');
      }
      if (err.code === 'auth/invalid-email') {
        Alert.alert('Dados incorretos');
      }
      console.log(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    navigation.setOptions({
      title: login ? 'Entrar' : 'Cadastrar',
    });
  }, [login]);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        Alert.alert('precisamos da localizacao');
        return;
      }

      let { coords } = await Location.getCurrentPositionAsync({});
      setLat(coords.latitude);
      setLon(coords.longitude);
    })();
    signInIfPossible();
  }, []);

  const signInIfPossible = async () => {
    try {
      const userUid = await SecureStorage.getItemAsync('user_uid');
      const userCpf = await SecureStorage.getItemAsync('user_cpf');
      const userPass = await SecureStorage.getItemAsync('user_pass');
      if (userUid && userPass && userCpf) {
        console.log('Com credenciais');
        console.log('cpf: ', userCpf);
        console.log('pass: ', userPass);
        const user = await auth.signInWithEmailAndPassword(
          `${userCpf}@easycoop.com`,
          userPass,
        );
        const producerId = user.user?.uid;
        navigation.dispatch(StackActions.replace('ProductsHome', { producerId }));
      } else {
        setIsLoading(false);
      }
    } catch (error) {
      console.log(error);
      Alert.alert(error);
    }
  }

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      const dt = new Date();
      const fn = dt.toISOString();
      const im = await fetch(result.uri);
      const bb = await im.blob();

      const uploadTask = fb.storage().ref(`producers/${fn}`).put(bb);
      uploadTask.on(
        'state_changed',
        s => {},
        e => {
          console.log(e);
        },
        () => {
          fb.storage()
            .ref('producers')
            .child(`${fn}`)
            .getDownloadURL()
            .then(u => setImageUri(u));
        },
      );
    }
  };

  return isLoading ? (
    <View
      style={{
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <ActivityIndicator color={colors.mainGreen} size="large" />
    </View>
  ) : (
    <>
      <AppBar
        navigation={navigation}
        canGoBack={false}
        canLogout={false}
        title={login ? "Entrar" : "Cadastrar"}
      />
      <View style={style.Container}>
        <View style={style.Main}>
          {!login && (
            <>
              <View style={style.Img}>
                <ImageBackground
                  resizeMode="cover"
                  source={{ uri: imageUri }}
                  style={style.BgImg}>
                  <Text style={style.SelImg} onPress={() => pickImage()}>
                    Selecionar Imagem
                  </Text>
                </ImageBackground>
              </View>
              <TextInput
                style={style.Input}
                autoCapitalize="words"
                placeholder="Nome"
                value={name}
                onChangeText={setName}
              />
            </>
          )}
          <TextInput
            style={style.Input}
            placeholder="CPF"
            value={cpfMask(cpf)}
            onChangeText={e => setCpf(cpfMask(e))}
            keyboardType="numeric"
          />
          <TextInput
            style={style.Input}
            placeholder="Senha"
            autoCompleteType="password"
            secureTextEntry
            value={pass}
            onChangeText={setPass}
          />

          {!login && (
            <View>
              <Text
                style={{
                  textAlign: 'center',
                  margin: '3%',
                }}>
                {`Localizacao: ${lat},${lon}`}
              </Text>
            </View>
          )}
          <View style={style.Btn}>
            {login ? (
              <Button
                title="Nao possui conta?"
                onPress={() => setLogin(false)}
              />
            ) : (
              <Button title="Ja possui conta?" onPress={() => setLogin(true)} />
            )}
          </View>
          <View style={style.Btn}>
            {login ? (
              <Button
                disabled={isLoading}
                title="Login"
                color="#2eb364"
                onPress={() => onFormLoginSubmit()}
              />
            ) : (
              <Button
                disabled={isLoading}
                title="Cadastrar"
                color="#2eb364"
                onPress={() => onFormSubmit()}
              />
            )}
          </View>
        </View>
      </View>
    </>
  );
};

export default ProducerForm;
