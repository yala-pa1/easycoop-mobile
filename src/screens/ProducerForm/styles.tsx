import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  Container: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: '5%',
    paddingTop: '5%',
    backgroundColor: '#ddd',
  },
  Main: {
    borderRadius: 15,
    height: '100%',
    width: '100%',
    backgroundColor: '#fff',
    overflow: 'hidden',
    // display: 'flex',
    // flexDirection: 'row'
  },
  SelImg: {
    width: '100%',
    alignSelf: 'flex-end',
    backgroundColor: 'rgba(0,0,0, .2)',
    color: 'white',
    padding: 5,
    paddingLeft: 10,
  },
  Img: {
    flexDirection: 'row',
    height: 200,
    width: '100%',
    backgroundColor: '#2eb364',
  },
  BgImg: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  Input: {
    height: 40,
    margin: 10,
    marginBottom: 0,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#ddd',
    color: 'black',
    paddingLeft: 10,
  },
  Btn: {
    padding: 10,
    paddingBottom: 5,
  },
});
