import { numberMask } from '../../utils/masks';

describe("Mask", () => {
    test("With number", () => {
        expect(numberMask("200")).toBe("2,00");
    })
    test("With string", () => {
        expect(numberMask("a5500")).toBe("55,00");
    })
    test("With empty value", () => {
        expect(numberMask("")).toBe("0,00");
    })
})