import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import {
  Platform,
  Text,
  View,
  TextInput,
  ImageBackground,
  Button,
  Alert,
} from 'react-native';
import { RouteProp, useNavigation } from '@react-navigation/native';
// import API from '../../services/api';
import * as ImagePicker from 'expo-image-picker';
import fb from '../../services/firebase';

import { useState } from 'react';
import style from './styles';
import { StackNavigationProp } from '@react-navigation/stack';
import { StackScreens } from '../../routes';
import AppBar from '../../components/AppBar';

type Props = {
  route: RouteProp<StackScreens, 'ProductTypeForm'>;
  navigation: StackNavigationProp<StackScreens>;
};

const ProductTypeForm: React.FC<Props> = ({ navigation, route }) => {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [imageUri, setImageUri] = useState('none');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
    setName(route.params?.productTypeName || name);
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      const dt = new Date();
      const fn = dt.toISOString();
      const im = await fetch(result.uri);
      const bb = await im.blob();
      setLoading(true);
      const uploadTask = fb.storage().ref(`images/${fn}`).put(bb);
      uploadTask.on(
        'state_changed',
        s => {},
        e => {
          console.log(e);
        },
        () => {
          fb.storage()
            .ref('images')
            .child(`${fn}`)
            .getDownloadURL()
            .then(u => {
              setLoading(false);
              setImageUri(u)
            });
        },
      );
    }
  };

  const onFormSubmit = async () => {
    if(!(imageUri && name && description)) {
      Alert.alert('Preencha o Nome');
      return;
    }
    // if (!(imageUri && name)) {
    // Alert.alert('Preencha o Nome');
    //   return;
    // }

    try {
      setLoading(true);
      const db = fb.firestore();
      db.collection('types').add({
        name,
        description,
        imageUrl: imageUri,
        active: true,
      });
      Alert.alert('Sucesso', 'Cadastro realizado com sucesso!');
      navigation.navigate('ProductForm');
    } catch (error) {
      console.error(error);
      Alert.alert('Erro', 'Não foi possível realizar o cadastro');
    }
    setLoading(false);
  };

  return (
    <>
      <AppBar
        title="Cadastrar Tipo de Produto"
        canGoBack={true}
        canLogout={false}
        navigation={navigation}
      />
      <View style={style.Container}>
      <View style={style.Main}>
        <View style={style.Img}>
          <ImageBackground
            resizeMode="cover"
            source={{ uri: imageUri }}
            style={style.BgImg}>
            <Text style={style.SelImg} onPress={() => pickImage()}>
              Selecionar Imagem
            </Text>
          </ImageBackground>
        </View>


        <TextInput
          style={style.Input}
          placeholder="Nome"
          value={name}
          onChangeText={setName}
        />

        <TextInput
          style={style.Input}
          placeholder="Descrição"
          value={description}
          onChangeText={setDescription}
        />

        <View style={style.Btn}>
          <Button
            disabled={loading}
            title="Cadastrar"
            color="#2eb364"
            onPress={() => onFormSubmit()}
          />
        </View>
      </View>
    </View>
    </>
  );
};

export default ProductTypeForm;
