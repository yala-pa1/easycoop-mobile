export type ProductType = {
  _id: string;
  name: string;
  decription: string;
  imageUrl: string;
};

export type ProductTypeDetails = Omit<ProductType, '_id'>;
