type Producer = {
  _id: string;
  userId: string;
  name: string;
  cpf: string;
  location: {
    latitude: string;
    longitude: string;
  };
  logoImg?: string;
};

export type ProducerDetails = Omit<Producer, '_id'>;
