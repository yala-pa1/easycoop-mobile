import { ProductType } from './ProductType';

export type Product = {
  _id: string;
  producerId: string;
  description: string;
  amountType?: string;
  amountValue?: number;
  mass?: number;
  quantity?: number;
  productType?: string;
  imgURL?: string;
  name?: string;
  active?: boolean;
  date: Date;
};

export type ProductDetails = Omit<Product, '_id'>;
