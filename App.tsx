import 'react-native-gesture-handler';
import React from 'react';
import { LogBox, StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import AuthRoutes from './src/routes';

const App: React.FC = () => {
  console.disableYellowBox = true;
  return (
    <NavigationContainer>
      <StatusBar barStyle="light-content" />
      <AuthRoutes />
    </NavigationContainer>
  );
};

export default App;
